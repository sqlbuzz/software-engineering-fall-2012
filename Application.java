
import java.sql.SQLException;
import java.util.ArrayList;


public class Application {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	ArrayList <QuanPrice> quanPriceObj = new ArrayList<QuanPrice>();
	public static void main(String[] args) throws SQLException  {
		
		Application application = new Application();
		
		//flag when set to true prints all the error messages
		//use it for debugging purposes only.
		boolean flag = true;
		
		//application.createProduct(1, 90002, false, "Apple6", false);
		//application.loadProduct(1, flag);
		//application.removeProduct(8, flag);
		application.loadPriceScheme(90001, flag);
		//application.storeProduct(2, 1, 90001, "Software Engineering 2nd edition", flag);
		//application.createPriceScheme("Test Sunday", flag);
		
		//application.removePriceScheme(90019, flag);
		
		///application.storePriceScheme(90014, 1, 40, "sunday night", flag);
				
		
	}
	//creates a product
	public void createProduct(int categoryID, int pricingSchemeID, boolean isTaxable, String productDesc, boolean flag)
	{
		Operations operation = new Operations();
		
		
		try {
			//operation.createProduct(categoryID, pricingSchemeID, productDesc, isTaxable);
			System.out.println("ProductID: " + operation.createProduct(categoryID, pricingSchemeID, productDesc, isTaxable, flag).toString() + " created successfully.");
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			//System.out.println("");
			
		}
		
	}
	//Loads the product
	public void loadProduct(int ProductID, boolean flag)
	{
		Operations operation = new Operations();
		operation.loadProduct(ProductID, flag);
		
		
				
		
	}
	//Removes the product
	public void removeProduct(int ProductID, boolean flag)
	{
		
		Operations operation = new Operations();
		try {
			operation.removeProduct(ProductID, flag);
			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());
			System.out.println("Product could not be removed.");
		}
	}	
	
	//Update Product
	public void storeProduct(int ProductID, int CategoryID, int PricingSchemeID, String ProductDesc, boolean IsTaxable) throws SQLException
	{
		Operations op = new Operations();
		
		op.storeProduct(ProductID, CategoryID, PricingSchemeID, ProductDesc, IsTaxable, true);
		
	}
	
	
	
	//removes the pricing scheme
	public void removePriceScheme(int priceSchemeID, boolean flag) throws SQLException
	{
		Operations operation = new Operations();
		
		operation.removePriceScheme(priceSchemeID, flag);
		
				
	}
	
	
	//creates a pricing scheme
	public void createPriceScheme(String PricingSchemeDesc, boolean flag) throws SQLException
	{
		Operations operation = new Operations();
		QuanPrice qp = new QuanPrice();
		qp.setQuantity(1);
		qp.setPrice(10.0);
		quanPriceObj.add(qp);
		QuanPrice qp1=  new QuanPrice();
		qp1.setQuantity(2);
		qp1.setPrice(20.0);
		quanPriceObj.add(qp1);
		QuanPrice qp2=  new QuanPrice();
		qp2.setQuantity(3);
		qp2.setPrice(0.0);
		quanPriceObj.add(qp2);
		operation.createPriceScheme(quanPriceObj, PricingSchemeDesc, flag);
		//System.out.println("Last Inserted PriceSchemeID was: " + operation.createPriceScheme(Quantity, Price, PricingSchemeDesc, flag).toString());
		
	}
	
	
	
	//loads a pricing scheme
	public void loadPriceScheme(int PriceSchemePricesID, boolean flag)
	{
		Operations op = new Operations();
		op.loadPriceScheme(PriceSchemePricesID, flag);
		
		
	}
	
	//updates the existing pricing scheme
	public void storePriceScheme(int priceSchemeID, int Quantity, float Price, String PricingSchemeDesc, boolean flag) throws SQLException
	{
		Operations op = new Operations();
		
		op.storePriceScheme(priceSchemeID, Quantity, Price, PricingSchemeDesc, flag);
	}
	
	
}