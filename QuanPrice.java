
public class QuanPrice {
	private Integer Quantity;
	private Double Price;
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return Quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		Quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return Price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		Price = price;
	}
	
	
}
