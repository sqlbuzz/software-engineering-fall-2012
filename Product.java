
public class Product implements IProduct
{
	private Integer categoryID;
	private Integer PricingSchemeID;
	private String productDescription;
	private boolean IsTaxable;
	Integer productID;
	private String categoryDesc;
	

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}
	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	/**
	 * @return the categoryID
	 */
	public Integer getCategoryID() {
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}
	/**
	 * @return the isTaxable
	 */
	public boolean isIsTaxable() {
		return IsTaxable;
	}
	/**
	 * @param isTaxable the isTaxable to set
	 */
	public void setIsTaxable(boolean isTaxable) {
		IsTaxable = isTaxable;
	}
	/**
	 * @return the pricingSchemeID
	 */
	public Integer getPricingSchemeID() {
		return PricingSchemeID;
	}
	/**
	 * @param pricingSchemeID the pricingSchemeID to set
	 */
	public void setPricingSchemeID(Integer pricingSchemeID) {
		PricingSchemeID = pricingSchemeID;
	}
	/**
	 * @return the productID
	 */
	public Integer getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	/**
	 * @return the categoryDesc
	 */
	public String getCategoryDesc() {
		return categoryDesc;
	}
	/**
	 * @param categoryDesc the categoryDesc to set
	 */
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	
	
}